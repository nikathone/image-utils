FROM registry.gitlab.com/nikathone/image-utils/docker:19.03.8

ARG COMPOSE_VERSION=1.25.4

RUN curl -L --fail https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/run.sh -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose
