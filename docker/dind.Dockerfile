FROM registry.gitlab.com/nikathone/image-utils/docker:19.03.8

# https://github.com/docker/docker/blob/master/project/PACKAGERS.md#runtime-dependencies
RUN set -eux; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
    btrfs-progs \
    e2fsprogs \
    iptables \
    openssl \
    uidmap \
    xz-utils \
# pigz: https://github.com/moby/moby/pull/35697 (faster gzip implementation)
    pigz \
	; \
# only install zfs if it's available for the current architecture
	if zfs="$(apt-cache depends zfsutils-linux)" && [ -n "$zfs" ]; then \
		apt-get install -y --no-install-recommends zfsutils-linux; \
	fi

# set up subuid/subgid so that "--userns-remap=default" works out-of-the-box
RUN set -x \
    && addgroup --system dockremap \
    && adduser --system --ingroup dockremap \
      --gecos "dockremap user" dockremap \
      --home /nonexistent --shell /bin/false \
	  && echo 'dockremap:165536:65536' >> /etc/subuid \
	  && echo 'dockremap:165536:65536' >> /etc/subgid

# https://github.com/docker/docker/tree/master/hack/dind
ENV DIND_COMMIT 37498f009d8bf25fbb6199e8ccd34bed84f2874b

RUN set -eux; \
	wget -O /usr/local/bin/dind "https://raw.githubusercontent.com/docker/docker/${DIND_COMMIT}/hack/dind"; \
	chmod +x /usr/local/bin/dind

COPY --from=docker:19.03.8-dind /usr/local/bin/dockerd-entrypoint.sh /usr/local/bin/dockerd-entrypoint.sh

VOLUME /var/lib/docker
EXPOSE 2375 2376

ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
